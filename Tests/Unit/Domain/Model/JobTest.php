<?php

namespace Drk\DrkJobboard\Domain\Model;

use TYPO3\TestingFramework\Core\Unit\UnitTestCase;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Kay Strobach <typo3@kay-strobach.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
/**
 * Test case for class \Drk\DrkJobboard\Domain\Model\Job.
 *
 * @version $Id$
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @package TYPO3
 * @subpackage DRK Stellenbörse
 *
 * @author Kay Strobach <typo3@kay-strobach.de>
 */
class JobTest extends UnitTestCase
{

    /**
     * @var Job
     */
    protected $fixture;

    public function setUp()
    {
        $this->fixture = new Job();
    }

    public function tearDown()
    {
        unset($this->fixture);
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->fixture->setTitle('Conceived at T3CON10');

        $this->assertSame(
            'Conceived at T3CON10',
            $this->fixture->getTitle()
        );
    }

    /**
     * @test
     */
    public function getEmployerReturnsInitialValueForString()
    {
    }

    /**
     * @test
     */
    public function setEmployerForStringSetsEmployer()
    {
        $this->fixture->setEmployer('Conceived at T3CON10');

        $this->assertSame(
            'Conceived at T3CON10',
            $this->fixture->getEmployer()
        );
    }

    /**
     * @test
     */
    public function getVacancyReturnsInitialValueForString()
    {
    }

    /**
     * @test
     */
    public function setVacancyForStringSetsVacancy()
    {
        $this->fixture->setVacancy('Conceived at T3CON10');

        $this->assertSame(
            'Conceived at T3CON10',
            $this->fixture->getVacancy()
        );
    }

    /**
     * @test
     */
    public function getContactReturnsInitialValueForString()
    {
    }

    /**
     * @test
     */
    public function setContactForStringSetsContact()
    {
        $this->fixture->setContact('Conceived at T3CON10');

        $this->assertSame(
            'Conceived at T3CON10',
            $this->fixture->getContact()
        );
    }

    /**
     * @test
     */
    public function getContactMailReturnsInitialValueForString()
    {
    }

    /**
     * @test
     */
    public function setContactMailForStringSetsContactMail()
    {
        $this->fixture->setContactMail('Conceived at T3CON10');

        $this->assertSame(
            'Conceived at T3CON10',
            $this->fixture->getContactMail()
        );
    }

    /**
     * @test
     */
    public function getOwnerReturnsInitialValueForTx_Extbase_Domain_Model_FrontendUser()
    {
    }

    /**
     * @test
     */
    public function setOwnerForTx_Extbase_Domain_Model_FrontendUserSetsOwner()
    {
    }

    /**
     * @test
     */
    public function getVacancyTypeReturnsInitialValueForTx_DrkJobboard_Domain_Model_VacancyType()
    {
    }

    /**
     * @test
     */
    public function setVacancyTypeForTx_DrkJobboard_Domain_Model_VacancyTypeSetsVacancyType()
    {
    }

    /**
     * @test
     */
    public function getLocationReturnsInitialValueForTx_Zipcodes_Domain_Model_ZipCode()
    {
    }

    /**
     * @test
     */
    public function setLocationForTx_Zipcodes_Domain_Model_ZipCodeSetsLocation()
    {
    }

    /**
     * @test
     */
    public function search()
    {
    }
}
