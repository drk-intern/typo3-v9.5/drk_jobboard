<?php
if (!defined('TYPO3')) {
    die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'drk_jobboard',
    'Configuration/TypoScript',
    'DRK Jobboard'
);

// register flexform for drkjobboard search
$pluginSignature = 'drkjobboard_drkjobboardsearch';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages,recursive';
// register flexform for drkjobboard list
$pluginSignature = 'drkjobboard_drkjobboardlist';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages,recursive';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $pluginSignature,
    'FILE:EXT:drk_jobboard/Configuration/FlexForms/flexform_drkjobboard_list.xml'
);
// register flexform for drkjobboard list
$pluginSignature = 'drkjobboard_drkjobboardapplication';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages,recursive';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $pluginSignature,
    'FILE:EXT:drk_jobboard/Configuration/FlexForms/flexform_drkjobboard_application.xml'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'DrkJobboard',
    'DrkJobboardSearch',
    'DRK Jobboard Suche'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'DrkJobboard',
    'DrkJobboardList',
    'DRK Jobboard Liste'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'DrkJobboard',
    'DrkJobboardMail',
    'DRK Jobboard Mail'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'DrkJobboard',
    'DrkJobboardApplication',
    'DRK Jobboard Bewerbung'
);
