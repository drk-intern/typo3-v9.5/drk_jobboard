<?php

$GLOBALS['TCA']['tx_drkjobboard_domain_model_job'] = array(
    'ctrl' => array(
        'title' => 'LLL:EXT:drk_jobboard/Resources/Private/Language/locallang_db.xlf:tx_drkjobboard_domain_model_job',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => array(
            'starttime' => 'starttime',
            'endtime' => 'endtime',
            'disabled' => 'hidden',
        ),
        'security' => [
            'ignorePageTypeRestriction' => true
        ],
        'searchFields' => 'title,keywords,employer,vacancy,contact,contact_mail,owner,vacancy_type,location,',
        'iconfile' => 'EXT:drk_jobboard/Resources/Public/Icons/tx_drkjobboard_domain_model_job.gif'
    ),
    'types' => array(
        '1' => array('showitem' => 'title,--palette--;;2,vacancy,--palette--;;3,--div--;LLL:EXT:drk_jobboard/Resources/Private/Language/locallang_db.xlf:access,hidden,--palette--;;1,starttime,endtime'),
    ),
    'palettes' => array(
        '1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource'),
        '2' => array('showitem' => 'employer,city,zip,contact_mail,contact'),
        '3' => array('showitem' => 'location_international,vacancy_type,keywords'),
    ),
    'columns' => array(
        'sys_language_uid' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => array(
                'type' => 'language',
                'renderType' => 'selectSingle',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => array(
                    array('LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages', -1),
                    array('LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.default_value', 0)
                ),
            ),
        ),
        'l10n_parent' => array(
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => array(
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => array(
                    array('', 0),
                ),
                'foreign_table' => 'tx_drkjobboard_domain_model_job',
                'foreign_table_where' => 'AND tx_drkjobboard_domain_model_job.pid=###CURRENT_PID### AND tx_drkjobboard_domain_model_job.sys_language_uid IN (-1,0)',
            ),
        ),
        'l10n_diffsource' => array(
            'config' => array(
                'type' => 'passthrough',
            ),
        ),
        't3ver_label' => array(
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            )
        ),
        'hidden' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
            'config' => array(
                'type' => 'check',
            ),
        ),
        'starttime' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => array(
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => time(),
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ),
        ),
        'endtime' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => array(
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => strtotime('+2 month'),
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ),
        ),
        'crdate' => array(
            'exclude' => 1,
            'label' => 'Creation date',
            'config' => array(
                'type' => 'none',
                'format' => 'date',
                'eval' => 'date',
                'default' => time(),
            )
        ),
        'title' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:drk_jobboard/Resources/Private/Language/locallang_db.xlf:tx_drkjobboard_domain_model_job.title',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ),
        ),
        'employer' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:drk_jobboard/Resources/Private/Language/locallang_db.xlf:tx_drkjobboard_domain_model_job.employer',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ),
        ),
        'vacancy' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:drk_jobboard/Resources/Private/Language/locallang_db.xlf:tx_drkjobboard_domain_model_job.vacancy',
            'config' => array(
                'type' => 'text',
                'enableRichtext' => true,
                'cols' => 40,
                'rows' => 15,
            ),
        ),
        'contact' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:drk_jobboard/Resources/Private/Language/locallang_db.xlf:tx_drkjobboard_domain_model_job.contact',
            'config' => array(
                'type' => 'text',
                'cols' => 20,
                'rows' => 5,
                'eval' => 'trim,required'
            ),
        ),
        'contact_mail' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:drk_jobboard/Resources/Private/Language/locallang_db.xlf:tx_drkjobboard_domain_model_job.contact_mail',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ),
        ),
        'vacancy_type' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:drk_jobboard/Resources/Private/Language/locallang_db.xlf:tx_drkjobboard_domain_model_job.vacancy_type',
            'config' => array(
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_drkjobboard_domain_model_vacancytype',
                'minitems' => 0,
                'maxitems' => 1,
            ),
        ),
        'zip' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:drk_jobboard/Resources/Private/Language/locallang_db.xlf:tx_drkjobboard_domain_model_job.zip',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'int'
            ),
        ),
        'city' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:drk_jobboard/Resources/Private/Language/locallang_db.xlf:tx_drkjobboard_domain_model_job.city',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ),
        ),
        'location_international' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:drk_jobboard/Resources/Private/Language/locallang_db.xlf:tx_drkjobboard_domain_model_job.location_international',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ),
        ),
        'keywords' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:drk_jobboard/Resources/Private/Language/locallang_db.xlf:tx_drkjobboard_domain_model_job.keywords',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ),
        ),
    ),
);
