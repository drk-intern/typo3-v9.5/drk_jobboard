<?php

namespace Drk\DrkJobboard\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Kay Strobach <typo3@kay-strobach.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Drk\DrkJobboard\Domain\Repository\JobRepository;
use Drk\DrkJobboard\Domain\Repository\VacancyTypeRepository;
use Drk\DrkJobboard\View\PdfView;
use GeorgRinger\NumberedPagination\NumberedPagination;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Extbase\Http\ForwardResponse;
use Drk\DrkJobboard\Domain\Model\Job;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Pagination\QueryResultPaginator;
use \TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Extbase\Annotation\Inject;
use \TYPO3\CMS\Extbase\Annotation as TYPO3;


/**
 *
 *
 * @package drk_jobboard
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class JobController extends ActionController
{
    /**
     * jobRepository
     *
     * @var JobRepository
     */
    public $jobRepository;

    /**
     * VacancyTypeRepository
     *
     * @var \Drk\DrkJobboard\Domain\Repository\VacancyTypeRepository
     */
    public $vacancyTypeRepository;

    /**
     * Ugly!!! Bugfix as page.headerData is cleared in the template, who does such a shit!
     */
    public function initializeAlohaEditor()
    {
    }

    /**
     * Init
     *
     * @return void
     */
    protected function initializeAction()
    {
        // @todo use correct injection
        $this->jobRepository = GeneralUtility::makeInstance(JobRepository::class);
        $this->vacancyTypeRepository = GeneralUtility::makeInstance(VacancyTypeRepository::class);

        parent::initializeAction();

        $this->siteRelPath = PathUtility::getAbsoluteWebPath(ExtensionManagementUtility::extPath($this->request->getControllerExtensionKey()));
        $this->pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);

    }

    /**
     * @param ?string $sortingField
     * @param ?string $sortingOrder
     * @param ?int $howMany
     * @param ?int $currentPage
     */
    public function listAction(?string $sortingField = 'title', ?string $sortingOrder = 'DESC', ?int $howMany = 10, ?int $currentPage = 1): ResponseInterface
    {
        $this->settings['itemsPerPage'] = (int)$howMany;

        // Selektierung nach Stellentypen für eine Kategorisierte Ausgabe inkl. Sortierreihenfolge, by Stefan Bublies@dt-internet.de
        $jobs = $this->jobRepository->findBySorting();
        if ((int)($this->settings['vacancyType'] ?? 0) > '0') {
            $jobs = $this->jobRepository->findByVacancy();
        }

        $paginator = new QueryResultPaginator(
            $jobs,
            $currentPage,
            $this->settings['itemsPerPage']
        );

        $pagination = new NumberedPagination($paginator, 15);

        $this->view->assign('pagination',
            [
                'pagination' => $pagination,
                'paginator' => $paginator,
            ]);
        $this->view->assign('sortingField', $sortingField);
        $this->view->assign('sortingOrder', $sortingOrder);
        $this->view->assign('settings', $this->settings);

        return $this->htmlResponse();
    }

    /**
     * action show
     *
     * @param Job|null $job
     * @return void
     * @TYPO3\IgnoreValidation("job")
     *
     */
    public function showAction(Job $job = null): ResponseInterface
    {
        if ($job === null) {
            return (new ForwardResponse('list'))
                ->withControllerName('Job')
                ->withExtensionName('drk_jobboard')
                ;
        }
        if ($this->settings['showEnglishLabels'] === 1) {
            // Sprache auf EN setzen, wenn im Plugin englische Labels erzwungen werden sollen
            $sLangTmp = $GLOBALS['TSFE']->config['config']['language'];
            $GLOBALS['TSFE']->config['config']['language'] = 'en';
        }

        if ($this->settings['showEnglishLabels'] == 1) {
            // Sprache wieder zurücksetzen
            $GLOBALS['TSFE']->config['config']['language'] = $sLangTmp;
        }

        $this->view->assign('job', $job);

        return $this->htmlResponse();
    }

    /**
     * action preview
     *
     * @param Job $job
     * @TYPO3\IgnoreValidation("job")
     *
     * @return void
     */
    public function previewAction(Job $job): ResponseInterface
    {
        $this->view->assign('job', $job);

        return $this->htmlResponse();
    }

    /**
      * Initialize the pdf action. * This action uses the PdfView::class as view.
      */
    protected function initializePdfAction(): void
    {
        $this->defaultViewObjectName = PdfView::class;
    }

    /**
     * action show
     *
     * @param Job $job
     * @TYPO3\IgnoreValidation("job")
     *
     * @return void
     */
    public function pdfAction(Job $job): ResponseInterface
    {
        $this->view->assign('job', $job);
        $this->view->assign('settings', $this->settings);
        $this->view->assign('PAGENO', '{PAGENO}');
        $this->view->assign('nbpg', '{nbpg}');

        return $this->responseFactory->createResponse()
            ->withHeader('Content-Type', 'application/pdf')
            ->withBody($this->streamFactory->createStream($this->view->render()));
    }

    /**
     * action search
     *
     * @param string $distance
     * @param string $what
     * @param string $where
     * @param string $sortingField
     * @param string $sortingOrder
     * @param int $howMany
     *
     * @return void
     */
    public function searchAction(
        $distance = '10000',
        $what = '',
        $where = '',
        $sortingField = 'title',
        $sortingOrder = 'DESC',
        $howMany = 25
    ): ResponseInterface
    {
        // enforce geo search
        $demands = array(
            'contains' => array(
                'title' => $what,
                'vacancyType' => $this->settings['vacancyType'],
                'zipCodes' => null,
                'locationInternational' => null,
            ),
            'sorting' => array(
                'field' => $sortingField,
                'order' => $sortingOrder,
            ),
        );
        $this->settings['itemsPerPage'] = intval($howMany, 10) ? intval($howMany, 10) : 25;

        if ((int)$this->settings['showEnglishLabels'] === 1) {
            // Sprache auf EN setzen, wenn im Plugin englische Labels erzwungen werden sollen
            $sLangTmp = $GLOBALS['TSFE']->config['config']['language'];
            $GLOBALS['TSFE']->config['config']['language'] = 'en';
        }

        if ((int)$this->settings['showEnglishLabels'] === 1) {
            // Sprache wieder zurücksetzen
            $GLOBALS['TSFE']->config['config']['language'] = $sLangTmp;
        }


        $sortingFields = array(
            'starttime' => LocalizationUtility::translate('date', 'drk_jobboard'),
            'title' => LocalizationUtility::translate('title', 'drk_jobboard'),
            'location' => LocalizationUtility::translate('location', 'drk_jobboard'),
            'employer' => LocalizationUtility::translate('employer', 'drk_jobboard'),
        );

        $this->view->assign('id', $GLOBALS["TSFE"]->id);
        $this->view->assign('howMany', $howMany);
        $this->view->assign('what', $what);
        $this->view->assign('where', $where);
        $this->view->assign('distance', $distance);
        $this->view->assign('sortingField', $sortingField);
        $this->view->assign('sortingOrder', $sortingOrder);
        $this->view->assign('settings', $this->settings);
        $this->view->assign('jobs', $this->jobRepository->findByDemands($demands));
        $this->view->assign('jobsAutoComplete', $this->jobRepository->findAll());
        $this->view->assign('sortingFields', $sortingFields);
        $this->view->assign('allJobs', $this->jobRepository->findAll());

        return $this->htmlResponse();
    }
}
