<?php

namespace Drk\DrkJobboard\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Stefan Bublies <bublies@dt-internet.de>
 *  (c) 2024 Kay Strobach <ks@4viewture.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Drk\DrkJobboard\Domain\Dto\Candidate;
use Drk\DrkJobboard\Domain\Repository\JobRepository;
use Drk\DrkJobboard\Domain\Model\Job;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use TYPO3\CMS\Core\Mail\FluidEmail;
use TYPO3\CMS\Core\Mail\MailerInterface;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Extbase\Http\ForwardResponse;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/**
 *
 *
 * @package drk_jobboard
 *
 *
 */
class ApplicationController extends ActionController
{
    /**
     * jobRepository
     *
     * @var JobRepository
     */
    protected $jobRepository;

    public function __construct(
        private readonly LoggerInterface $logger,
    ) {}

    protected function initializeAction()
    {
        parent::initializeAction();

        // @todo use correct injection
        $this->jobRepository = GeneralUtility::makeInstance(JobRepository::class);

        $this->siteRelPath = PathUtility::getAbsoluteWebPath(ExtensionManagementUtility::extPath($this->request->getControllerExtensionKey()));
        $this->pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);

        $this->pageRenderer->addCssFile($this->siteRelPath . 'Resources/Public/Css/application.css');

        if ($this->arguments->hasArgument('candidate')) {
            $pmc = $this->arguments->getArgument('candidate')->getPropertyMappingConfiguration();
            $pmc->skipProperties('upload1', 'upload2', 'upload3');
        }
    }

    /**
     * action list
     *
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("job")
     * @param Job $job
     * @return void
     */
    public function listAction(Job $job = null): ResponseInterface
    {
        if ($job === null) {
            return (new ForwardResponse('list'))
                ->withControllerName('Job')
                ->withExtensionName('drk_jobboard')
                ;
        }

        if (filter_var($this->settings['privacyUrl'], FILTER_VALIDATE_URL)) {
            $this->view->assign('privacy_url', $this->settings['privacyUrl']);
        }

        $candidate = new Candidate();
        $candidate->setJob($job);

        $this->view->assign('candidate', $candidate);
        $this->view->assign('job', $job);

        return $this->htmlResponse();
    }

    /**
     * action send
     *
     * @param Candidate $candidate
     * @return void
     */
    public function sendAction(Candidate $candidate): ResponseInterface
    {
        $this->logger->info(
            'Accessing send method for candidate {candidate_vorname} {candidate_nachname} {candidate_mail}',
            [
                'candidate_vorname' => $candidate->getVorname(),
                'candidate_nachname' => $candidate->getNachname(),
                'candidate_mail' => $candidate->getMail(),
            ]
        );
        $candidate->initializeFileObjects($_FILES);

        // Damit keine leeren Formulare abgesendet werden koennen, by Stefan Bublies@dt-internet.de
        # if ($_REQUEST['tx_drkjobboard_drkjobboardapplication']['JavascriptTest'] !== 'klappt') {
        #     $errorMessages = 'Dieses Formular erfordert JavaScript. Um es korrekt nutzen zu k&ouml;nnen, aktivieren Sie bitte JavaScript in Ihrem Browser';
        #     $this->view->assign('errorMessages', $errorMessages);
        #     return $this->htmlResponse();
        # }

        $this->sendMail($candidate, $this->settings);

        $candidate->cleanUp();

        $this->htmlResponse();
        return $this->redirectToPid($this->settings['danke']);
    }

    protected function sendMail(Candidate $candidate, array $settings)
    {
        $sizeCollector = 0;

        /** @var FluidEmail $email */
        $email = GeneralUtility::makeInstance(FluidEmail::class);
        $email
            ->to($candidate->getJob()->getContactMail())
            ->replyTo($candidate->getMail())
            ->subject('Neue Bewerbung für ' . $candidate->getJob()->getTitle())
            ->format(FluidEmail::FORMAT_BOTH) // send HTML and plaintext mail
            ->setTemplate('JobApplication')
            ->assign('candidate', $candidate)
            ->assign('normalizedParams', $this->request->getAttribute('normalizedParams'));

        if ($settings['sender'] !== '') {
            $email->sender($settings['sender']);
        }

        for ($i = 1; $i <= 3; $i++) {
            $method = 'getUpload' . $i;
            $file = $candidate->$method();
            if ($file instanceof File) {
                if ($file->getSize() > ($settings['maxsizeperfile'] * 1024 * 1024)) {
                    throw new \InvalidArgumentException('The file ' . $file->getName() . ' is too big.');
                }
                $email->attach(
                    $file->getContents(),
                    $file->getName(),
                    $file->getMimeType()
                );
            }

        }

        if ($sizeCollector > ($settings['maxsizeallfiles'] * 1024 * 1024)) {
            throw new \InvalidArgumentException('The combined file sizes ' . $file->getName() . ' exceed the limit.');
        }

        GeneralUtility::makeInstance(MailerInterface::class)?->send($email);
    }

    protected function redirectToPid(int $pageUid, int $statusCode = 303): ResponseInterface
    {
        $this->uriBuilder->reset()->setCreateAbsoluteUri(true);
        if (MathUtility::canBeInterpretedAsInteger($pageUid)) {
            $this->uriBuilder->setTargetPageUid((int)$pageUid);
        }
        if (GeneralUtility::getIndpEnv('TYPO3_SSL')) {
            $this->uriBuilder->setAbsoluteUriScheme('https');
        }
        $uri = $this->uriBuilder->uriFor('', [], null, null);
        return $this->redirectToUri($uri, null, $statusCode);
    }
}
