<?php

namespace Drk\DrkJobboard\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Kay Strobach <typo3@kay-strobach.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\Configuration\ConfigurationManager;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Extbase\Persistence\QueryInterface;
use \TYPO3\CMS\Extbase\Persistence\Repository;

/**
 *
 *
 * @package drk_jobboard
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class JobRepository extends DisRespectStoragePidRepository
{
    /**
     * @var array
     */
    protected $defaultOrderings = [
        'title' => QueryInterface::ORDER_ASCENDING
    ];

    protected $whiteList = array(
        'sorting' => array(
            'title',
            'uid',
            'starttime',
            'stoptime',
            'employer',
            'location.placeName',
            'location_international'
        )
    );

    /**
     * Finds all by pid
     *
     * @return QueryResultInterface The images
     */
    public function findAllForEditing()
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectEnableFields(false);
        return $query->execute();
    }

    /**
     * @param $zips
     *
     * @return array|QueryResultInterface
     */
    public function findAllByZips($zips)
    {
        $query = $this->createQuery();
        $query->matching($query->in('zip', $zips));
        return $query->execute();
    }

    /**
     * @param null|mixed $demands
     *
     * @return array|QueryResultInterface
     */
    public function findByDemands($demands = null)
    {
        // just catch no settings by simply returning everything :D
        #$demands = NULL;
        if ($demands === null) {
            return $this->findAll();
        } else {
            $constraints = array();
            $query = $this->createQuery();
            if ($demands['contains']['title'] !== '') {
                $keywords = GeneralUtility::trimExplode(' ', $demands['contains']['title']);
                $keywordConstraints = array();
                $keywordConstraints[] = $query->like('title', '%' . $demands['contains']['title'] . '%');
                if (is_array($keywords)) {
                    foreach ($keywords as $keyword) {
                        $keywordConstraints[] = $query->like('keywords', '%' . $keyword . '%');
                    }
                    $constraints[] = $query->logicalOr($keywordConstraints);
                }
            }
            if ($demands['contains']['vacancyType']) {
                $constraints[] = $query->equals('vacancyType', $demands['contains']['vacancyType']);
            }
            if ($demands['contains']['zipCodes']) {
                // Drewitz 15076
                $subConstraints = [];
                /* @var \Fourviewture\Zipcodes\Domain\Model\ZipCode $zipCode */
                foreach ($demands['contains']['zipCodes'] as $zipCode) {
                    $subConstraints[] = $query->equals('zip', $zipCode->getPostalCode());
                }

                $constraints[] = $query->logicalOr($subConstraints);
            }

            if (isset($demands['contains']['owner'])) {
                $constraints[] = $query->equals('owner', $demands['contains']['owner']);
            }

            if (in_array($demands['sorting']['field'], $this->whiteList['sorting'], true)) {
                $query->setOrderings(
                    array(
                        $demands['sorting']['field'] => $demands['sorting']['order'] === 'DESC' ?
                            QueryInterface::ORDER_DESCENDING :
                            QueryInterface::ORDER_ASCENDING,
                    )
                );
            }

            if (count($constraints) >= 1) {
                $query->matching(
                    $query->logicalAnd(
                        $constraints
                    )
                );
            }

            return $query->execute();
        }
    }

    /**
     *
     *
     * @return array|QueryResultInterface
     */
    public function findByVacancy()
    {
        $configurationManager = GeneralUtility::makeInstance(ConfigurationManager::class);
        $aConfig = $configurationManager->getConfiguration(
            ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS,
            'drkjobboard',
            'drkjobboardlist'
        );

        $query = $this->createQuery();
        switch ($aConfig['sortby'] ?? 'dateasc') {
            case 'dateasc':
                $query->setOrderings(array('starttime' => QueryInterface::ORDER_ASCENDING));
                break;
            case 'datedesc':
                $query->setOrderings(array('starttime' => QueryInterface::ORDER_DESCENDING));
                break;
            case 'name':
                $query->setOrderings(array('title' => QueryInterface::ORDER_ASCENDING));
                break;
        }

        return $query->execute();
    }

    /**
     *
     *
     * @return array|QueryResultInterface
     */
    public function findBySorting()
    {
        $configurationManager = GeneralUtility::makeInstance(ConfigurationManager::class);
        $aConfig = $configurationManager->getConfiguration(
            ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS,
            'drkjobboard',
            'drkjobboardlist'
        );
        $query = $this->createQuery();
        switch ($aConfig['sortby'] ?? 'dateasc') {
            case 'dateasc':
                $query->setOrderings(array('starttime' => QueryInterface::ORDER_ASCENDING));
                break;
            case 'datedesc':
                $query->setOrderings(array('starttime' => QueryInterface::ORDER_DESCENDING));
                break;
            case 'name':
                $query->setOrderings(array('title' => QueryInterface::ORDER_ASCENDING));
                break;
        }
        return $query->execute();
    }
}
