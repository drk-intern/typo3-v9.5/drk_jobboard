<?php
/**
 * Created by PhpStorm.
 * User: kpurrmann
 * Date: 18.02.16
 * Time: 13:01
 */

namespace Drk\DrkJobboard\Domain\Repository;

use TYPO3\CMS\Core\Utility\ClassNamingUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\QuerySettingsInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

class DisRespectStoragePidRepository extends Repository
{
    /**
     * Constructs a new Repository
     */
    public function __construct()
    {
        parent::__construct();
        $this->initializeObject();
    }

    /**
     *
     */
    public function initializeObject()
    {
        $querySettings = GeneralUtility::makeInstance(QuerySettingsInterface::class);
        $querySettings->setRespectStoragePage(false);
        $this->setDefaultQuerySettings($querySettings);
    }
}
