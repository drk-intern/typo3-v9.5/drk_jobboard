<?php

namespace Drk\DrkJobboard\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Kay Strobach <typo3@kay-strobach.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Annotation as Extbase;

/**
 *
 *
 * @package drk_jobboard
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Job extends AbstractEntity
{

    /**
     * Title
     *
     * @var string
     * @Extbase\Validate("NotEmpty")
     */
    protected $title;

    /**
     * employer
     *
     * @var string
     * @Extbase\Validate("NotEmpty")
     */
    protected $employer;

    /**
     * Vacancy Title
     *
     * @var string
     * @Extbase\Validate("NotEmpty")
     */
    protected $vacancy;

    /**
     * Contact
     *
     * @var string
     * @Extbase\Validate("NotEmpty")
     */
    protected $contact;

    /**
     * Contact Email
     *
     * @var string
     * @Extbase\Validate("NotEmpty")
     */
    protected $contactMail;

    /**
     * @var \DateTime
     */
    protected $crdate;

    /**
     * @var \DateTime
     * @Extbase\Validate("DateTime")
     */
    protected $starttime;

    /**
     * @var \DateTime
     * @Extbase\Validate("DateTime")
     */
    protected $endtime;

    /**
     * Type of vacancy
     *
     * @var VacancyType
     * @Extbase\ORM\Lazy
     */
    protected $vacancyType;


    /**
     * @var int
     */
    protected $zip;


    /**
     * @var string
     */
    protected $city;

    /**
     * Location international
     *
     * @var string
     */
    protected $locationInternational;

    /**
     * keywords
     *
     * @var string
     */
    protected $keywords;

    /**
     * Job constructor.
     * Fix DateTime Bugs!
     *
     * @param array $data
     */
    public function __construct(array $data = array())
    {
//        $this->starttime = new \DateTime('@' . strtotime('now'));
//        $this->endtime = new \DateTime('@' . strtotime('+2 month'));
//        $this->crdate = new \DateTime('@' . strtotime('now'));
//        foreach ($data as $attribute => $value) {
//            $this->_setProperty($attribute, $value);
//        }

        // Damit Validierung greit und ein User nicht versehentlich das Prefill-Date herauslöscht und sich dann über das Löschen seines Stellenangebotes nach 2 Monaten wundert
        if ($_POST['tx_drkjobboard_search']['job']) {
            if ($_POST['tx_drkjobboard_search']['job']['starttime'] === '') {
                $this->starttime = '';
            }
            if ($_POST['tx_drkjobboard_search']['job']['endtime'] === '') {
                $this->endtime = '';
            }
        }
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     *
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the employer
     *
     * @return string $employer
     */
    public function getEmployer()
    {
        return $this->employer;
    }

    /**
     * Sets the employer
     *
     * @param string $employer
     *
     * @return void
     */
    public function setEmployer($employer)
    {
        $this->employer = $employer;
    }

    /**
     * Returns the vacancy
     *
     * @return string $vacancy
     */
    public function getVacancy()
    {
        return $this->vacancy;
    }

    /**
     * Sets the vacancy
     *
     * @param string $vacancy
     *
     * @return void
     */
    public function setVacancy($vacancy)
    {
        $this->vacancy = $vacancy;
    }

    /**
     * Returns the contact
     *
     * @return string $contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Sets the contact
     *
     * @param string $contact
     *
     * @return void
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * Returns the contactMail
     *
     * @return string $contactMail
     */
    public function getContactMail()
    {
        return $this->contactMail;
    }

    /**
     * Sets the contactMail
     *
     * @param string $contactMail
     *
     * @return void
     */
    public function setContactMail($contactMail)
    {
        $this->contactMail = $contactMail;
    }

    /**
     * Returns the crdate
     *
     * @return \DateTime $crdate
     */
    public function getCrdate()
    {
        return $this->crdate;
    }

    /**
     * Sets the crdate
     *
     * @param \DateTime $crdate
     *
     * @return void
     */
    public function setCrdate(\DateTime $crdate)
    {
        $this->crdate = $crdate;
    }

    /**
     * Returns the endtime
     *
     * @return \DateTime $endtime
     */
    public function getEndtime()
    {
        return $this->endtime;
    }

    /**
     * Sets the endtime
     *
     * @param \DateTime $endtime
     *
     * @return void
     */
    public function setEndtime(\DateTime $endtime)
    {
        $this->endtime = $endtime;
    }

    /**
     * Returns the starttime
     *
     * @return \DateTime $starttime
     */
    public function getStarttime()
    {
        return $this->starttime;
    }

    /**
     * Sets the starttime
     *
     * @param \DateTime $starttime
     *
     * @return void
     */
    public function setStarttime(\DateTime $starttime)
    {
        $this->starttime = $starttime;
    }

    /**
     * Returns the vacancyType
     *
     * @return VacancyType $vacancyType
     */
    public function getVacancyType()
    {
        return $this->vacancyType;
    }

    /**
     * Sets the vacancyType
     *
     * @param VacancyType $vacancyType
     *
     * @return void
     */
    public function setVacancyType(VacancyType $vacancyType)
    {
        $this->vacancyType = $vacancyType;
    }

    /**
     * @return int
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param int $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * Returns the keywords
     *
     * @return string $keywords
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Sets the keywords
     *
     * @param string $keywords
     *
     * @return void
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
    }

    /**
     * @return string
     */
    public function getLocationInternational()
    {
        return $this->locationInternational;
    }

    /**
     * @param string $locationInternational
     */
    public function setLocationInternational($locationInternational)
    {
        $this->locationInternational = $locationInternational;
    }
}
