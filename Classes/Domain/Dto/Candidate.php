<?php

namespace Drk\DrkJobboard\Domain\Dto;

use Composer\DependencyResolver\GenericRule;
use Drk\DrkJobboard\Domain\Model\Job;
use TYPO3\CMS\Core\Resource\DuplicationBehavior;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Resource\StorageRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;

class Candidate
{
    /**
     * @var Job
     */
    protected Job $job;
    protected string $anrede = '';

    protected string $vorname = '';

    protected string $nachname = '';

    protected string $geburtstag = '';

    protected string $plz = '';

    protected string $stadt = '';

    protected string $anschrift = '';

    protected string $telefon = '';

    protected string $mail = '';

    protected string $ausbildung = '';

    protected string $info = '';

    protected ?File $upload1 = null;

    protected ?File $upload2 = null;

    protected ?File $upload3 = null;


    public function getJob(): Job
    {
        return $this->job;
    }

    public function setJob(Job $job): Candidate
    {
        $this->job = $job;
        return $this;
    }

    public function getAnrede(): string
    {
        return $this->anrede;
    }

    public function setAnrede(string $anrede): Candidate
    {
        $this->anrede = $anrede;
        return $this;
    }

    public function getVorname(): string
    {
        return $this->vorname;
    }

    public function setVorname(string $vorname): Candidate
    {
        $this->vorname = $vorname;
        return $this;
    }

    public function getNachname(): string
    {
        return $this->nachname;
    }

    public function setNachname(string $nachname): Candidate
    {
        $this->nachname = $nachname;
        return $this;
    }

    public function getGeburtstag(): string
    {
        return $this->geburtstag;
    }

    public function setGeburtstag(string $geburtstag): Candidate
    {
        $this->geburtstag = $geburtstag;
        return $this;
    }

    public function getPlz(): string
    {
        return $this->plz;
    }

    public function setPlz(string $plz): Candidate
    {
        $this->plz = $plz;
        return $this;
    }

    public function getStadt(): string
    {
        return $this->stadt;
    }

    public function setStadt(string $stadt): Candidate
    {
        $this->stadt = $stadt;
        return $this;
    }

    public function getAnschrift(): string
    {
        return $this->anschrift;
    }

    public function setAnschrift(string $anschrift): Candidate
    {
        $this->anschrift = $anschrift;
        return $this;
    }

    public function getTelefon(): string
    {
        return $this->telefon;
    }

    public function setTelefon(string $telefon): Candidate
    {
        $this->telefon = $telefon;
        return $this;
    }

    public function getMail(): string
    {
        return $this->mail;
    }

    public function setMail(string $mail): Candidate
    {
        $this->mail = $mail;
        return $this;
    }

    public function getAusbildung(): string
    {
        return $this->ausbildung;
    }

    public function setAusbildung(string $ausbildung): Candidate
    {
        $this->ausbildung = $ausbildung;
        return $this;
    }

    public function getInfo(): string
    {
        return $this->info;
    }

    public function setInfo(string $info): Candidate
    {
        $this->info = $info;
        return $this;
    }

    public function getUpload1(): ?File
    {
        return $this->upload1;
    }

    /**
     * @param ?File $upload1
     * @return $this
     */
    public function setUpload1(?File $upload1): Candidate
    {
        $this->upload1 = $upload1;
        return $this;
    }

    public function getUpload2(): ?File
    {
        return $this->upload2;
    }

    /**
     * @param ?File $upload2
     * @return $this
     */

    public function setUpload2(?File $upload2): Candidate
    {
        $this->upload2 = $upload2;
        return $this;
    }

    public function getUpload3(): ?File
    {
        return $this->upload3;
    }

    /**
     * @param ?File $upload1
     * @return $this
     */
    public function setUpload3(?File $upload3): Candidate
    {
        $this->upload3 = $upload3;
        return $this;
    }

    /**
     * Adds an uploaded file into the Storage.
     * contains information about the uploaded file given by $_FILES['file1']
     *
     * @param string $property
     * @param string|null $tmpName
     * @param string $name
     * @return void
     */
    public function setImportTempfile(string $property, string $name, string $tmpName): void
    {
        $storageRepository = GeneralUtility::makeInstance(StorageRepository::class);
        $storage = $storageRepository->getDefaultStorage();
        $targetFolder = $storage->getDefaultFolder();

        /** @var \TYPO3\CMS\Core\Resource\File $file */
        $file = $storage->addFile(
            $tmpName,
            $targetFolder,
            $name,
            DuplicationBehavior::RENAME
        );

        $method = 'set' . ucfirst($property);
        $this->$method($file);
    }

    public function getAsArray()
    {
        return [
            'job' => $this->getJob()->getTitle(),
            'jobContact' => $this->getJob()->getContactMail(),
            'anrede' => $this->getAnrede(),
            'vorname' => $this->getVorname(),
            'nachname' => $this->getNachname(),
            'geburtstag' => $this->getGeburtstag(),
            'plz' => $this->getPlz(),
            'stadt' => $this->getStadt(),
            'anschrift' => $this->getAnschrift(),
            'telefon' => $this->getTelefon(),
            'mail' => $this->getMail(),
            'ausbildung' => $this->getAusbildung(),
            'info' => $this->getInfo(),
            'upload1' => $this->getUpload1()?->getName(),
            'upload2' => $this->getUpload2()?->getName(),
            'upload3' => $this->getUpload3()?->getName(),
        ];
    }

    public function initializeFileObjects(array $files)
    {
        if (!empty($files['tx_drkjobboard_drkjobboardapplication']['name']['candidate']['upload1'])) {
            $this->setImportTempfile(
                'upload1',
                $files['tx_drkjobboard_drkjobboardapplication']['name']['candidate']['upload1'],
                $files['tx_drkjobboard_drkjobboardapplication']['tmp_name']['candidate']['upload1']
            );
        }
        if (!empty($files['tx_drkjobboard_drkjobboardapplication']['name']['candidate']['upload2'])) {
            $this->setImportTempfile(
                'upload2',
                $files['tx_drkjobboard_drkjobboardapplication']['name']['candidate']['upload2'],
                $files['tx_drkjobboard_drkjobboardapplication']['tmp_name']['candidate']['upload2']
            );
        }
        if (!empty($files['tx_drkjobboard_drkjobboardapplication']['name']['candidate']['upload3'])) {
            $this->setImportTempfile(
                'upload3',
                $files['tx_drkjobboard_drkjobboardapplication']['name']['candidate']['upload3'],
                $files['tx_drkjobboard_drkjobboardapplication']['tmp_name']['candidate']['upload3']
            );
        }
    }

    public function cleanUp()
    {
        $this->upload1?->delete();
        $this->upload2?->delete();
        $this->upload3?->delete();
    }
}
