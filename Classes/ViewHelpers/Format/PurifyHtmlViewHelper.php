<?php

namespace Drk\DrkJobboard\ViewHelpers\Format;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

class PurifyHtmlViewHelper extends AbstractViewHelper
{
    /**
     * Specifies whether the escaping interceptors should be disabled or enabled for the result of renderChildren() calls within this ViewHelper
     * @see isChildrenEscapingEnabled()
     *
     * Note: If this is NULL the value of $this->escapingInterceptorEnabled is considered for backwards compatibility
     *
     * @var boolean
     * @api
     */
    protected $escapeChildren = false;
    /**
     * Specifies whether the escaping interceptors should be disabled or enabled for the render-result of this ViewHelper
     * @see isOutputEscapingEnabled()
     *
     * @var boolean
     * @api
     */
    protected $escapeOutput = false;

    /**
     * @return string
     * @throws \Exception
     */
    public function render()
    {
        $value = $this->arguments['value'];
        if (!class_exists('\HTMLPurifier_Bootstrap')) {
            require_once ExtensionManagementUtility::extPath('drk_jobboard') . 'Resources/Private/PHP/vendor/autoload.php';
        }
        \HTMLPurifier_Bootstrap::registerAutoload();
        $config = \HTMLPurifier_Config::createDefault();
        $config->set('URI.AllowedSchemes', array('data' => true));
        $purifier = \HTMLPurifier::getInstance($config);
        if ($value === null) {
            return $purifier->purify($this->renderChildren());
        } else {
            return $purifier->purify($value);
        }
    }

    public function initializeArguments(): void
    {
        $this->registerArgument('value', 'string', '', false);
    }
}
