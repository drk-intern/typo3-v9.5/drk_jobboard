<?php

namespace Drk\DrkJobboard\ViewHelpers;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractConditionViewHelper;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Context\Context;
/**
 * Class AccessViewHelper
 *
 * @package Drk\DrkJobboard\ViewHelpers
 */
class AccessViewHelper extends AbstractConditionViewHelper
{
    /**
     * AccessViewHelper constructor.
     */
    public function __construct()
    {
        $this->registerArgument('userId', 'int', '', true);
    }

    /**
     * This method decides if the condition is TRUE or FALSE. It can be overriden in extending viewhelpers to adjust functionality.
     *
     * @param array $arguments ViewHelper arguments to evaluate the condition for this ViewHelper, allows for flexiblity in overriding this method.
     * @return bool
     */
    protected static function evaluateCondition($arguments = null)
    {
        return isset($GLOBALS['TSFE']) && GeneralUtility::makeInstance(Context::class)->getPropertyFromAspect('frontend.user', 'isLoggedIn') && ($GLOBALS['TSFE']->fe_user->user['uid'] === (int)$arguments['userId']);
    }
}
