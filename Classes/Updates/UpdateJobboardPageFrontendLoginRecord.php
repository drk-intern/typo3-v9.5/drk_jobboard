<?php
declare(strict_types=1);

namespace Drk\DrkJobboard\Updates;

use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;
use TYPO3\CMS\Install\Updates\DatabaseUpdatedPrerequisite;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

/**
 * update login-record for jobboerse
 */
#[UpgradeWizard('drktemplate2016updateJobFeLoginRecord')]

class UpdateJobboardPageFrontendLoginRecord extends AbstractRecordUpdater implements UpgradeWizardInterface
{
    protected $table = 'tt_content';

    /**
     * @return string Title of this updater
     */
    public function getTitle(): string
    {
        return 'DRK-Template 2016: Remove tt_content record jobboard fe-login';
    }

    /**
     * @return string Longer description of this updater
     */
    public function getDescription(): string
    {
        return 'Adjust tt_content record, by hidding it';
    }

    /**
     * Performs the accordant updates.
     *
     * @return bool Whether everything went smoothly or not
     */
    public function executeUpdate(): bool
    {
        /** @var Connection $connection */
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable($this->table);
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $connection->createQueryBuilder();
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $cType = 'textmedia';
        $bodytext = 'Melden Sie sich bitte im Backend an: Ihre-Website/typo3. Nach dem Einloggen gelangen Sie auf die Stellenanzeigen.';
        $connection = $queryBuilder
            ->update('tt_content')
            ->where(
                $queryBuilder->expr()->eq('uid', 656)
            )
            ->set('CType', $cType)
            ->set('bodytext', $bodytext)
            ->execute();

        return true;
    }

    /**
     * Check if there are record exist
     *
     * @return bool
     * @throws \InvalidArgumentException
     */
    protected function checkIfWizardIsRequired(): bool
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $connectionPool->getQueryBuilderForTable($this->table);
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $numberOfEntries = $queryBuilder
            ->count('uid')
            ->from($this->table)
            ->where(
                $queryBuilder->expr()->eq('uid', 656)
            )
            ->andWhere(
                $queryBuilder->expr()->eq('hidden', 0),
                $queryBuilder->expr()->eq('deleted', 0)
            )
            ->executeQuery()
            ->fetchFirstColumn();
        return $numberOfEntries > 0;
    }
}
