<?php

namespace Drk\DrkJobboard\Hooks;

use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Type\ContextualFeedbackSeverity;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\PathUtility;

/**
 * Class ExtensionmanagerHook
 *
 * @package KayStrobach\Webdav\Hook
 */
class ExtensionmanagerHook
{
    /**
     * @return string
     */
    public function checkUrlFopen()
    {
        switch (ini_get('allow_url_fopen')) {
            case 0:
                $flashMessage = new FlashMessage(
                    'Your PHP Settings have are not sufficient, please set allow_url_fopen to on',
                    'Problematic Setting in url_fopen, loading external resources may fail!',
                    ContextualFeedbackSeverity::WARNING
                );
                break;
            case 1:
                $flashMessage = new FlashMessage(
                    'There should be no errors',
                    'Perfect allow_url_fopen is set to on',
                    ContextualFeedbackSeverity::OK
                );
                break;
            default:
                $flashMessage = new FlashMessage(
                    'Current Setting of allow_url_fopen = ' . ini_get('allow_url_fopen'),
                    'Unknown setting of allow_url_fopen',
                    ContextualFeedbackSeverity::ERROR
                );
        }
        return $flashMessage->getTitle();
    }

    public function isCurlEnabled()
    {
        if (function_exists('curl_version')) {
            $flashMessage = new FlashMessage(
                'There should be no problems loading external resources',
                'Curl is enabled',
                ContextualFeedbackSeverity::OK
            );
        } else {
            $flashMessage = new FlashMessage(
                'Curl is not enabled, loading external resources may fail!',
                'Curl not enabled',
                ContextualFeedbackSeverity::ERROR
            );
        }
        return $flashMessage->getTitle();
    }

    public function downloadCheck()
    {
        $testUri = $_SERVER["REQUEST_SCHEME"] . '://' . $_SERVER["SERVER_NAME"] . '/' . PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('drk_jobboard')) . 'Resources/Public/Images/logo-drk.svg';
        $buffer = @file_get_contents($testUri);
        $flashMessage1 = new FlashMessage(
            'Fetch URI: ' . htmlspecialchars($testUri),
            'Trying to fetch content',
            ContextualFeedbackSeverity::INFO
        );
        if ($buffer !== false) {
            $flashMessage = new FlashMessage(
                'Tried to fetch some content:<br>' . $buffer,
                'Fetched Content',
                ContextualFeedbackSeverity::INFO
            );
        } else {
            $error = error_get_last();
            if (!is_array($error)) {
                $error = [
                    'message' => 'message'
                ];
            }
            if (!is_array($http_response_header)) {
                $http_response_header = [
                    'no response headers',
                    'maybe dns problems, timeouts, or any other lowlevel problem'
                ];
            }
            $error = $error['message'] . '<br>' . implode('<br>', $http_response_header);
            $flashMessage = new FlashMessage(
                'HTTP request failed. Error was: ' . $error,
                'Error fetching content!',
                ContextualFeedbackSeverity::ERROR
            );
        }

        return $flashMessage1->getTitle() . $flashMessage->getTitle();
    }
}
