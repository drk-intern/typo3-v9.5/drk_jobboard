<?php

namespace Drk\DrkJobboard\View;

use GuzzleHttp\Psr7\Response;
use TYPO3\CMS\Fluid\View\TemplateView;
use Mpdf\Mpdf;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class PdfView extends TemplateView
{
    const AUTOLOADER = __DIR__ . '/../../Resources/Private/PHP/vendor/autoload.php';

    /**
     * File pattern for resolving the template file
     * Following placeholders are supported:
     * - "@templateRoot"
     * - "@partialRoot"
     * - "@layoutRoot"
     * - "@subpackage"
     * - "@action"
     * - "@format"
     *
     * @var string
     */
    protected string $templatePathAndFilenamePattern = '@templateRoot/@subpackage/@controller/@action.@format';

    protected $tempFilesPath = null;

    public function initializeView()
    {
        if (file_exists(self::AUTOLOADER)) {
            include_once self::AUTOLOADER;
        }

        $this->tempFilesPath = Environment::getPublicPath() . '/' . 'typo3temp/Cache/Data/drk_jobboard/mpdf/';
        GeneralUtility::mkdir_deep($this->tempFilesPath);

        if (!defined('_MPDF_TEMP_PATH')) {
            define('_MPDF_TEMP_PATH', $this->tempFilesPath);
        }
        GeneralUtility::mkdir_deep($this->tempFilesPath);
    }

    public function render($actionName = null)
    {
        $this->initializeView();

        $buffer = parent::render($actionName);

        if ($buffer === null) {
            $buffer = 'empty';
        }

        $pdfFilename = $this->tempFilesPath . hash('sha256', $buffer) . '.pdf';

        if (!file_exists($pdfFilename)) {
            $mpdf = new Mpdf(
                [
                    'setAutoTopMargin' => true,
                    'setAutoBottomMargin' => true,
                    'orientation' => 'P',
                    'tempDir' => $this->tempFilesPath
                ]
            );
            $mpdf->tempDir = $this->tempFilesPath;
            $mpdf->_setPageSize('A4', $orientation);
            $mpdf->WriteHTML($buffer);
            $mpdf->Output($pdfFilename, 'F');
        }

        return file_get_contents($pdfFilename);

        $relativeFileName = str_replace($_SERVER['DOCUMENT_ROOT'], '', $pdfFilename);
        header(
            'Location: ' . $_SERVER["REQUEST_SCHEME"] . '://' . $_SERVER["SERVER_NAME"] . '/' . $relativeFileName,
            true,
            302
        );

        return '';
    }
}
