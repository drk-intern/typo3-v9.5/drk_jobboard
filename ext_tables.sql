#
# Table structure for table 'tx_drkjobboard_domain_model_job'
#
CREATE TABLE tx_drkjobboard_domain_model_job
(

    uid                    int(11)                         NOT NULL auto_increment,
    pid                    int(11)             DEFAULT '0' NOT NULL,

    title                  varchar(255)        DEFAULT ''  NOT NULL,
    employer               varchar(255)        DEFAULT ''  NOT NULL,
    vacancy                text                            NOT NULL,
    contact                text                            NOT NULL,
    contact_mail           varchar(255)        DEFAULT ''  NOT NULL,
    keywords               varchar(255)        DEFAULT ''  NOT NULL,
    vacancy_type           int(11) unsigned    DEFAULT '0',
    zip                    int(5) unsigned     DEFAULT '0',
    city                   varchar(255)        DEFAULT ''  NOT NULL,
    location_international varchar(255)        DEFAULT ''  NOT NULL,

    tstamp                 int(11) unsigned    DEFAULT '0' NOT NULL,
    crdate                 int(11) unsigned    DEFAULT '0' NOT NULL,
    cruser_id              int(11) unsigned    DEFAULT '0' NOT NULL,
    deleted                tinyint(4) unsigned DEFAULT '0' NOT NULL,
    hidden                 tinyint(4) unsigned DEFAULT '0' NOT NULL,
    starttime              int(11) unsigned    DEFAULT '0' NOT NULL,
    endtime                int(11) unsigned    DEFAULT '0' NOT NULL,

    t3ver_oid              int(11)             DEFAULT '0' NOT NULL,
    t3ver_id               int(11)             DEFAULT '0' NOT NULL,
    t3ver_wsid             int(11)             DEFAULT '0' NOT NULL,
    t3ver_label            varchar(255)        DEFAULT ''  NOT NULL,
    t3ver_state            tinyint(4)          DEFAULT '0' NOT NULL,
    t3ver_stage            int(11)             DEFAULT '0' NOT NULL,
    t3ver_count            int(11)             DEFAULT '0' NOT NULL,
    t3ver_tstamp           int(11)             DEFAULT '0' NOT NULL,
    t3ver_move_id          int(11)             DEFAULT '0' NOT NULL,

    t3_origuid             int(11)             DEFAULT '0' NOT NULL,
    sys_language_uid       int(11)             DEFAULT '0' NOT NULL,
    l10n_parent            int(11)             DEFAULT '0' NOT NULL,
    l10n_diffsource        mediumblob,

    is_dummy_record        tinyint(1) unsigned DEFAULT '0' NOT NULL,

    PRIMARY KEY (uid),
    KEY parent (pid),
    KEY t3ver_oid (t3ver_oid, t3ver_wsid),
    KEY phpunit_dummy (is_dummy_record),
    KEY language (l10n_parent, sys_language_uid)

);

#
# Table structure for table 'tx_drkjobboard_domain_model_vacancytype'
#
CREATE TABLE tx_drkjobboard_domain_model_vacancytype
(

    uid              int(11)                         NOT NULL auto_increment,
    pid              int(11)             DEFAULT '0' NOT NULL,

    title            varchar(255)        DEFAULT ''  NOT NULL,

    tstamp           int(11) unsigned    DEFAULT '0' NOT NULL,
    crdate           int(11) unsigned    DEFAULT '0' NOT NULL,
    cruser_id        int(11) unsigned    DEFAULT '0' NOT NULL,
    deleted          tinyint(4) unsigned DEFAULT '0' NOT NULL,
    hidden           tinyint(4) unsigned DEFAULT '0' NOT NULL,
    starttime        int(11) unsigned    DEFAULT '0' NOT NULL,
    endtime          int(11) unsigned    DEFAULT '0' NOT NULL,

    t3ver_oid        int(11)             DEFAULT '0' NOT NULL,
    t3ver_id         int(11)             DEFAULT '0' NOT NULL,
    t3ver_wsid       int(11)             DEFAULT '0' NOT NULL,
    t3ver_label      varchar(255)        DEFAULT ''  NOT NULL,
    t3ver_state      tinyint(4)          DEFAULT '0' NOT NULL,
    t3ver_stage      int(11)             DEFAULT '0' NOT NULL,
    t3ver_count      int(11)             DEFAULT '0' NOT NULL,
    t3ver_tstamp     int(11)             DEFAULT '0' NOT NULL,
    t3ver_move_id    int(11)             DEFAULT '0' NOT NULL,

    t3_origuid       int(11)             DEFAULT '0' NOT NULL,
    sys_language_uid int(11)             DEFAULT '0' NOT NULL,
    l10n_parent      int(11)             DEFAULT '0' NOT NULL,
    l10n_diffsource  mediumblob,

    is_dummy_record  tinyint(1) unsigned DEFAULT '0' NOT NULL,

    PRIMARY KEY (uid),
    KEY parent (pid),
    KEY t3ver_oid (t3ver_oid, t3ver_wsid),
    KEY phpunit_dummy (is_dummy_record),
    KEY language (l10n_parent, sys_language_uid)

);
