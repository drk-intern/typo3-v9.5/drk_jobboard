<?php
if (!defined('TYPO3')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_jobboard',
    'DrkJobboardSearch',
    array(
        \Drk\DrkJobboard\Controller\JobController::class => 'search, list, new, create, edit, update, delete, show, pdf, preview'
    ),
    array(
        \Drk\DrkJobboard\Controller\JobController::class => 'search, list, new, create, edit, update, delete, show, pdf, preview'
    )
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_jobboard',
    'DrkJobboardList',
    array(
        \Drk\DrkJobboard\Controller\JobController::class => 'list, show, new, create, edit, update, delete, search, pdf, preview',
    ),
    array(
        \Drk\DrkJobboard\Controller\JobController::class => 'list, show, new, create, edit, update, delete, search, pdf, preview',
    )
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_jobboard',
    'DrkJobboardMail',
    array(
        \Drk\DrkJobboard\Controller\MailController::class => 'new, create',
    ),
    array(
        \Drk\DrkJobboard\Controller\MailController::class => 'new, create',
    )
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_jobboard',
    'DrkJobboardApplication',
    array(
        \Drk\DrkJobboard\Controller\JobController::class => 'list, show, new, create, edit, update, delete, search, pdf, preview',
        \Drk\DrkJobboard\Controller\ApplicationController::class => 'list, send',
    ),
    array(
        \Drk\DrkJobboard\Controller\JobController::class => 'list, show, new, create, edit, update, delete, search, pdf, preview',
        \Drk\DrkJobboard\Controller\ApplicationController::class => 'list, send',
    )
);

# Scheduler
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['Drk\DrkJobboard\Controller\AutodeleteController'] =
    array(
        'extension' => 'drk_jobboard',
        'title' => 'Stellenangebote löschen',
        'description' => 'Löscht veraltete Stellenangebote (anhand des End-Datums)'
    );

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['clearCachePostProc']['drk_jobboard'] = 'Drk\DrkJobboard\Hooks\T3libTcemainHook->clearCachePostProc';

// mailer config
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['templateRootPaths'][1707223498] = 'EXT:drk_jobboard/Resources/Private/Templates/Email/';
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['partialRootPaths'][1707223498] = 'EXT:drk_jobboard/Resources/Private/Partials/';
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['layoutRootPaths'][1707223498] = 'EXT:drk_jobboard/Resources/Private/Layouts/';
