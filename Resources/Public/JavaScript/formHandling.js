require(['jquery'], function ($) {
  $('#tx_jobboard_previewbutton').on('click', function (e) {
    e.preventDefault();

    var form = $(this).closest('form');

    var originalAction = form.attr('action');

    form.attr('action', $(this).attr('formaction'));
    form.attr('target', '_blank');

    form.submit();

    form.attr('action', originalAction);
    form.attr('target', '');

    return false;
  });

  $('a.jobDelete').bind('click', function () {
    return window.confirm('Wollen Sie die Stellenausschreibung wirklich löschen?');
  });
});

