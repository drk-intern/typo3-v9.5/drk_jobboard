tinymce.init({
  plugins: "fullscreen, link",
  selector: "#vacancy",
  browser_spellcheck: true,
  // das slash bei language url ist häßlich, aber da im live system base nicht gesetzt wird leider nötig ...
  language_url: '/typo3conf/ext/drk_jobboard/Resources/Public/JavaScript/TinyMce/Langs/de.js',
  element_format: "html",
  fix_list_elements: true,
  forced_root_block: 'p',
  toolbar: "undo redo | cut copy paste | bold bullist numlist outdent indent blockquote | link | fullscreen | removeformat",
  menubar: false,
  statusbar: false
});
