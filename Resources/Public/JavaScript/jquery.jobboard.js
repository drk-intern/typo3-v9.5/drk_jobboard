////////
//
//	copyright:	Stefan Bublies
//	internet:	www.dt-internet.de
//
////////


require(['jquery', 'jquery.validate'], function ($) {

  /////////////////////////////////////////////////////////////////////////////////////
  // Job Formular
  /////////////////////////////////////////////////////////////////////////////////////

  $('form#applicationform').attr("autocomplete", "off");

  $('form#applicationform').attr("novalidate", "novalidate");

  // validate signup form on keyup and submit
  $("form#applicationform").validate({

    errorPlacement: function (error, element) {
      // Append error within linked label
      $(element)
        .closest("form")
        .find("div[id='error_" + element.attr("id") + "']")
        .append(error);
    },
    errorClass: "uk-alert",
    errorElement: "div",
    validClass: "has-value success",


    rules: {
      "tx_drkjobboard_drkjobboardapplication[candidate][anrede]": {
        required: true
      },
      "tx_drkjobboard_drkjobboardapplication[candidate][vorname]": {
        required: true
      },
      "tx_drkjobboard_drkjobboardapplication[candidate][nachname]": {
        required: true
      },
      "tx_drkjobboard_drkjobboardapplication[candidate][geburtstag]": {
        required: true
      },

      "tx_drkjobboard_drkjobboardapplication[candidate][plz]": {
        required: true
      },
      "tx_drkjobboard_drkjobboardapplication[candidate][stadt]": {
        required: true
      },
      "tx_drkjobboard_drkjobboardapplication[candidate][anschrift]": {
        required: true
      },
      "tx_drkjobboard_drkjobboardapplication[candidate][telefon]": {
        required: true
      },
      "tx_drkjobboard_drkjobboardapplication[candidate][mail]": {
        required: true,
        email: true
      },
      "tx_drkjobboard_drkjobboardapplication[candidate][ausbildung]": {
        required: true
      },

    },

    //submitHandler: function (form) { // for demo
    //	alert('valid form submitted'); // for demo
    //  return false; // for demo
    //},

    messages: {
      "tx_drkjobboard_drkjobboardapplication[candidate][anrede]": {
        required: "Bitte tragen Sie Ihre Anrede ein",
        minlength: "Tragen Sie mindestens 2 Zeichen ein"
      },
      "tx_drkjobboard_drkjobboardapplication[candidate][vorname]": {
        required: "Bitte tragen Sie Ihren Vornamen ein",
        minlength: "Tragen Sie mindestens 3 Zeichen ein"
      },
      "tx_drkjobboard_drkjobboardapplication[candidate][nachname]": {
        required: "Bitte tragen Sie Ihren Nachnamen ein",
        minlength: "Tragen Sie mindestens 3 Zeichen ein"
      },
      "tx_drkjobboard_drkjobboardapplication[candidate][geburtstag]": {
        required: "Bitte tragen Sie Ihre Geburtsdatum ein",
        minlength: "Bitte überprüfen Sie Ihre Angaben"
      },
      "tx_drkjobboard_drkjobboardapplication[candidate][plz]": {
        required: "Bitte tragen Sie Ihre Postleitzahl ein",
        minlength: "Tragen Sie mindestens 5 Zeichen ein"
      },
      "tx_drkjobboard_drkjobboardapplication[candidate][stadt]": {
        required: "Bitte tragen Sie Ihren Wohnort ein",
        minlength: "Tragen Sie mindestens 3 Zeichen ein"
      },
      "tx_drkjobboard_drkjobboardapplication[candidate][anschrift]": {
        required: "Bitte tragen Sie Ihre Anschrift ein",
        minlength: "Tragen Sie mindestens 4 Zeichen ein"
      },
      "tx_drkjobboard_drkjobboardapplication[candidate][telefon]": {
        required: "Bitte tragen Sie Ihre Telefonnummer ein",
        minlength: "Tragen Sie mindestens 5 Zeichen ein"
      },
      "tx_drkjobboard_drkjobboardapplication[candidate][mail]": {
        required: "Bitte tragen Sie Ihre Mail-Adresse ein",
        minlength: "Bitte überprüfen Sie Ihre Angaben"
      },
      "tx_drkjobboard_drkjobboardapplication[candidate][ausbildung]": {
        required: "Bitte tragen Sie Ihre Ausbildung ein",
        minlength: "Tragen Sie mindestens 5 Zeichen ein"
      },
    }
  });

});
