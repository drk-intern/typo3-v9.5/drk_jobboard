<f:layout name="SystemEmail" />
<f:section name="Subject">Neue Bewerbung für {candidate.job.title}</f:section>
<f:section name="Title">Neue Bewerbung für {candidate.job.title}</f:section>
<f:section name="Main">
Es gab eine neue Bewerbung für die folgende Stelle:

{candidate.job.title}

Bitte prüfen Sie Anhänge vor dem Öffnen auf Viren.

<f:for each="{candidate.asArray}" as="value" key="key">
    --<f:translate id="fieldname-{key}" default="{key}" extensionName="drk_jobboard" key="fieldname-{key}"/>--

    {value}
</f:for>
</f:section>
