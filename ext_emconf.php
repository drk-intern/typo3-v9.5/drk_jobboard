<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "drk_jobboard".
 *
 * Auto generated 29-04-2014 14:06
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF['drk_jobboard'] = array(
    'title' => 'DRK Stellenbörse',
    'description' => 'Stellenbörsen Erweiterung',
    'category' => 'plugin',
    'author' => 'Kay Strobach / Kjeld Schumacher / Stefan Bublies',
    'author_email' => 'typo3@kay-strobach.de',
    'author_company' => '',
    'state' => 'beta',
    'version' => '12.10.0',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.0-10.4.99',
            'drk_general' => ''
        ],
        'conflicts' => [],
        'suggests' => [
            'drk_template2016' => ''
        ],
    ],
);
